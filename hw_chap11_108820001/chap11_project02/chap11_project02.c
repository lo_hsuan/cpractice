/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒(put your name here)                            */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.19 (put program development started date here   */
/* Purpose: find the closest departure time                      */
/* Change History: 1119                                          */
/*****************************************************************/
#include<stdio.h>

#define SIZE ( (int)(sizeof(departures)/sizeof(departures[0])))

void find_closest_flight(int desire_time, int *depature_time, int *arrival_time);

int main(void){
	//宣告變數
	int hours, minutes, desire_time, departure_time,
		departure_hour, arrival_time, arrival_hour;
	//讀入 
	printf("Enter a 24-hour time : ");
	scanf("%d : %d", &hours, &minutes);
	
	//turn into minutes
	desire_time = hours * 60 + minutes;
	
	find_closest_flight(desire_time, &departure_time, &arrival_time);
	
	printf("closest departure time is ");
	
	departure_hour = departure_time / 60;
	
	if(departure_hour == 0)
	    departure_hour = 12;
	else if (departure_hour > 12)       //轉12進制
	    departure_hour -= 12;
    
    printf("%d:%.2d ", departure_hour, departure_time % 60);
    
    //departure_hour < 12
    
    if(departure_time < 60 * 12 )          //determine a.m. or p.m.
        printf("a.m.");
    else    
        printf("p.m.");
        
    printf(" ,arriving at ");
    arrival_hour = arrival_time / 60;
    if(arrival_hour == 0){
        arrival_hour = 12;
    }else if(arrival_hour > 12){       //轉12進制
        arrival_hour -= 12;
    }    
    printf("%d:%.2d ", arrival_hour, arrival_time % 60);
    
    if(arrival_time < 60 * 12)           //determine a.m. or p.m.
        printf("a.m.");
    else 
        printf("p.m.");
        
    printf("\n");    
        
	return 0;
}

void find_closest_flight(int desired_time, int *departure_time, int *arrival_time){
    int departures[] = {480, 583, 679,767, 840, 945, 1140, 1305};
    int arrivals[] = {616, 712, 811, 900, 968, 1075, 1280, 1438};
    int closest;                         //存 index
    if(desired_time <= departures[0]){      //480那班
        closest = 0;
    }else if(desired_time > departures[SIZE - 1]){      //1305那班
        closest = SIZE - 1;        //SIZE = sizeof(departures) / sizeof(departures[0])
    }                             //SIZE = 32 / 4 = 8
    else{               
        closest = 0;
        while(desired_time > departures[closest+1]){    //departures中 取不大於desire_time 最大的值
            closest++;      //departures[closest],departures[closest+1]夾著desire_time
        }                       
        if((departures[closest + 1] - desired_time) < (desired_time - departures[closest])){
            closest++;      //離後面一個較近
        }
    }
    
    *departure_time = departures[closest];
    *arrival_time = arrivals[closest];
}

