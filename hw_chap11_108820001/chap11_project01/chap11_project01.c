/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.19 (put program development started date here   */
/* Purpose:  how to pay with bill                                */
/* Change History: 1119									         */
/*****************************************************************/

#include<stdio.h>
/* prototype */
void pay_amount(int money, int *twenties, int *tens, int *fives, int *ones);

int main(void){
	//宣告變數
	int money, twenties, tens, fives, ones;
	//讀入money
	printf("Enter a dollar amount: ");
	scanf("%d", &money);
	
	//傳入 twenties, tens, fives, ones 的位址 
	pay_amount( money , &twenties, &tens, &fives, &ones);
	printf("\n"); 
	//需幾張20元 
	printf("$20 bills: %d\n", twenties);					
	
	//需幾張10元 
	printf("$10 bills: %d\n", tens);
	
	//需幾張5元 
	printf("$5 bills: %d\n", fives);
	
	//需幾張1元 
	printf("$1 bills: %d\n", ones);
	
	return 0;
}
void pay_amount(int money, int *twenties, int *tens, int *fives, int *ones){
	*twenties = money / 20;		//20
	money %= 20;
	*tens = money / 10;			//10
	money %= 10;
	*fives = money / 5;			//5
	money %= 5;
	*ones =money;				//1
}