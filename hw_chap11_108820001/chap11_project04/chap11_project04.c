/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.16 (put program development started date here   */
/* Purpose: rpn                                                */
/* Change History: 1116                                          */
/*****************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define STACK_SIZE 100

/* globol variables */
int contents[STACK_SIZE];			//stack
int top = 0;							//records highest number
/* prototype */
void make_empty();
bool is_empty();
bool is_full();
void push(int i);
int pop();
void stack_overflow();
void stack_underflow();


int main(void){
	//宣告變數
	char ch;						//讀資料 
	int op1, op2;
	
	printf("Enter an RPN expression: ");
    for(;;){
        //input char
        scanf(" %c", &ch);
        
        switch(ch){
            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
                push(ch - '0');     // -'0' : char -> int (What we push in is the integer)
                break;
            case '+':
                push(pop()+pop());
                break;
            case '-':					
                op1=pop();
                op2=pop();
                push(op2-op1);
                break;
            case '*':
                push(pop()*pop());
                break;
            case '/':
                op1=pop();
                op2=pop();
                push(op2/op1);
                break;
            case '=':
                printf("Value of expression: %d\n", pop());
                make_empty();
                printf("Enter an RPN expression: ");
                break;
            default: 
                exit(EXIT_SUCCESS);
        }
    }
	
	return 0;
}

void make_empty(){		//make the stack empty
	top=0;
}
bool is_empty(){		//determine wherther we can pop
	return top == 0;
}
bool is_full(){			//determine whether we can push
	return top == STACK_SIZE ;
}
void push(int i){		//put into stack
	if(is_full()){
		stack_overflow();
	}else{
		contents[top++] = i;		//push into stack
	}
}
int pop(){				//throw out stack 
	if(is_empty()){
		stack_underflow();
		return '\0';				
	}else{
		return contents[--top];		//pop out topest number
	}
					
}
void stack_overflow(){
	printf("Expression is too complex\n");
	exit(EXIT_FAILURE);
}
void stack_underflow(){
	printf("Not enough oprands in expression\n");
	exit(EXIT_FAILURE);
}
