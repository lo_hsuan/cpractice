/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.10.13 (put program development started date here   */
/* Purpose: 化簡分數 					                         */
/* Change History: 1013                                          */
/*****************************************************************/
#include<stdio.h> 
int GCD(int ,int);		//function to find GCD of a,b
 
int main(void){
	//宣告變數
	int a;		//分子 
	int b;		//分母 
	int gcd;	//最大公因數 
	//讀入 
	printf("Enter a fraction: ");
	scanf("%d/%d",&a,&b);
	gcd=GCD(a,b);
	a=a/gcd;
	b=b/gcd;
	if(b<0){
		a=a*(-1);
		b=b*(-1);
	}
	//化簡並輸出	
	printf("In lowest terms: %d/%d" ,a ,b );	
	
	return 0;
}
int GCD(int a,int b){
	int k=a%b;		
	while(k!=0){	//輾轉相除法找GCD 
		a=b;
		b=k;
		k=a%b;
	}
	return b;
}