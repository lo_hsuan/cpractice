/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.14 (put program development started date here   */
/* Purpose: stack                                                */
/* Change History: 1114                                          */
/*****************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define STACK_SIZE 100
/* globol variables */
char contents[STACK_SIZE];			//stack
int top=0;							//records highest number
/* prototype */
void make_empty();
bool is_empty();
bool is_full();
void push(char ch);
char pop();
void stack_overflow();
void stack_underflow();


int main(void){
	//宣告變數
	bool properly_nested = true;
	char ch;						//讀資料 
	
	printf("Enter aprentheses and/or braces: ");
	while(properly_nested && (ch = getchar())!='\n'){
		if(ch == '(' || ch == '{'){				//push
			push(ch);
		}else if(ch == ')'){					//pop
			properly_nested = (pop() == '(');
		}else if(ch == '}'){
			properly_nested = (pop() =='{');
		}
	}
	
	if(properly_nested && is_empty()){			//stack is clear
		printf("Parentheses/braces are nested properly\n");
	}else{
		printf("Parentheses/braces are NOT nested properly\n");
	}
	
	return 0;
}

void make_empty(){		//make the stack empty
	top=0;
}
bool is_empty(){		//determine wherther we can pop
	return top == 0;
}
bool is_full(){			//determine whether we can push
	return top == STACK_SIZE;
}
void push(char ch){		//put into stack
	if(is_full()){
		stack_overflow();
	}else{
		contents[top++] = ch;		//push into stack
	}
}
char pop(){				//throw out stack 
	if(is_empty()){
		stack_underflow();
		return '\0';				
	}else{
		return contents[--top];		//pop out topest number
	}
					
}
void stack_overflow(){
	printf("Stack overflow\n");
	exit(EXIT_FAILURE);
}
void stack_underflow(){
	printf("Stack underflow\n");
	exit(EXIT_FAILURE);
}
