/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: ù�аa (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.15 (put program development started date here   */
/* Purpose: poker 			                                    */
/* Change History: 1115                                          */
/*****************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define NUM_RANKS 13
#define NUM_SUITS 4
#define NUM_CARDS 5

/*global variable*/
int hand[NUM_CARDS][2];
bool straight, flush, four, three;
int pairs;

/* prototype */
void read_cards();
bool duplicate_card(int rank, int suit, int hand[NUM_CARDS][2], int cards_read);
void analyze_hand();
void print_result();

int main(){
	for(;;){
		read_cards();
		analyze_hand();
		print_result();
	}
}

void read_cards(){	
	//declare variable
	char c,rank_ch,suit_ch;
	int rank,suit;
	bool bad_card;
	int cards_read=0;

	
	while(cards_read < NUM_CARDS){
		bad_card = false;
		//input
		printf("Enter a card: ");

		rank_ch = getchar();		//det rank
		switch(rank_ch){
            case '0':   exit(EXIT_SUCCESS);
            case '2':   rank = 0; break;
            case '3':   rank = 1; break;
            case '4':   rank = 2; break;
            case '5':   rank = 3; break;
            case '6':   rank = 4; break;
            case '7':   rank = 5; break;
            case '8':   rank = 6; break;
            case '9':   rank = 7; break;
            case 't':  case 'T': rank = 8; break;
            case 'j':  case 'J': rank = 9; break;
            case 'q':  case 'Q': rank = 10; break;
            case 'k':  case 'K': rank = 11; break;
            case 'a':  case 'A': rank = 12; break;
            default: bad_card=true;
		}

		suit_ch = getchar();		//get suit
		switch(suit_ch){
			case 'c': case 'C': suit=0; break;
			case 'd': case 'D': suit=1; break;
			case 'h': case 'H': suit=2; break;
			case 's': case 'S': suit=3; break;
			default: bad_card=true; 
		}

		//input error
		while(( c=getchar() ) != '\n')
			if(c != ' ') bad_card = true;

		if(bad_card)						//detecting input error
			printf("Bad card; ignored.\n");
		else if(duplicate_card(rank, suit, hand, cards_read))	//calling
			printf("Duplicate card; ignored.\n");
		else {								//if carrect : read next
			hand[cards_read][0] = rank;
			hand[cards_read][1] = suit;
			cards_read++;
		}
	}
}

//check duplicate card
bool duplicate_card(int rank,int suit,int hand[NUM_CARDS][2], int cards_read){
	int i;
	for(i=0 ; i<cards_read ; i++){
		if(rank==hand[i][0] && suit==hand[i][1])	
			return true;
	}	
	return false;
}

// determine what type of card we have

void analyze_hand(){
	//declare variable
	int num_consec=0;
	int card, rank, matches;
	int tmp;
	int i, j, smallest, temp_suit, temp_rank;

	straight = false;
	flush = false;
	four = false;
	three = false;
	pairs = 0;

	// sort card
	for(i=0 ; i<NUM_CARDS ; i++){
		smallest=i;
		for(j = i+1 ; j<NUM_CARDS ; j++){
			if(hand[j][0] < hand[smallest][0])	
				smallest=j;						//find smallest rank
		}
		
		temp_rank=hand[i][0];					//swap
		temp_suit=hand[i][1];
		hand[i][0]=hand[smallest][0];
		hand[i][1]=hand[smallest][1];
		hand[smallest][0]=temp_rank;
		hand[smallest][1]=temp_suit;
	}

	// check for flush
	for(card=1 ; card<NUM_CARDS ; card++){
		if(hand[card][1] != hand[0][1])
			break;
		if(card==NUM_CARDS-1)
			flush=true;
	}

	// check for straight
	for(card = 1;card<NUM_CARDS;card++){
		if(hand[card][0]-hand[card-1][0] != 1)
			break;
		if(card == NUM_CARDS-1)
			straight = true;
	}

	// check four-of-a-kind, three-of-a-kind,  pairs
	for(i=0 ; i<NUM_CARDS ; i++){
		matches=0;
		for(j=i+1 ; j<NUM_CARDS ; j++)
			if(hand[j][0] == hand[i][0])
				matches++;

		if(matches == 1) pairs++;
		if(matches == 2) three = true;
		if(matches == 3) four = true;
	}
}

//print result
void print_result(){
	if(straight && flush) 		printf("Straight flush");
	else if(four) 				printf("Four of a kind");
	else if(three && pairs) 	printf("Full House");
	else if(flush) 				printf("Flush");
	else if(straight) 			printf("Straight");
	else if(three) 				printf("Three of a kind");
	else if(pairs==2) 			printf("Two pairs");
	else if(pairs==1) 			printf("Pair");
	else 						printf("High card");
	
	printf("\n\n");
}