/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.07 (put program development started date here   */
/* Purpose: caculate power                                       */
/* Change History: 1107                                          */
/*****************************************************************/
#include<stdio.h> 
/*function prototype*/
int power(int,int);

int main(void){
	int x,n;
	//input
	printf("For x^n, enter x: ");
	scanf("%d", &x);
	printf("For x^n, enter n: ");
	scanf("%d", &n);
	//output
	printf("%d^%d = %d\n", x, n, power(x, n));	//calling
	
	return 0;
}
/*function definition*/
int  power(int x,int n){
	if(n == 0)
		return 1;		//base case
	
	if(n%2 == 0)		//n is even
		return power(x, n/2) * power(x, n/2);
	else				//n is odd
		return x * power(x, n-1);

}
