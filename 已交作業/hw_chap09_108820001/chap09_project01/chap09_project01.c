/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.03 (put program development started date here   */
/* Purpose: selection sort                                       */
/* Change History: 1103                                          */
/*****************************************************************/
#include<stdio.h>

/*funtion prototype*/
void selection_sort(int [], int);

int main(void){
	//宣告變數
	int num[100];	
	int i,n;
	char c;
	
	//input
	printf("Enter a list of intergers: ");
	for(i=0,n=0 ; ; i++){
	    scanf("%d",&num[i]);
	    n++;
	    if((c = getchar())=='\n'){      //以換行作為結束(其餘為'\0')
	        break;
	    }
	    ungetc(c,stdin);    //將一個字符退回字串流
	}
	selection_sort(num , n);       //calling
    //output
    for(i=0; i<n ;i++){
        printf("%d ",num[i]);
    }
    printf("\n");
	
	return 0;
}
/* funtion definition */
void selection_sort(int num[], int n){
    int i,j;
    int max, maxindex;				//to record maximum
    int temp;
	if (n==1){
	   /*exit function*/
	}else{
	    j = n-1;
		max = -2147483648;
		for(i=j ; i>=0 ; i--){      //find largest number
			if(num[i] > max){
				max = num[i];
				maxindex = i;
			}
		}
		//swap
		temp = num[maxindex];
		num[maxindex] = num[j];
		num[j] = temp;
	
		return selection_sort(num, --n);	//recursion
	}
}
