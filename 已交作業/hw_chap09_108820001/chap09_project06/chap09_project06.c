/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.07 (put program development started date here   */
/* Purpose: caculate polynomial                                  */
/* Change History: 1107                                          */
/*****************************************************************/
#include<stdio.h> 
/*funcyion prototype*/
int polynomial(int);
int main(void){
	int x;
	//input
	printf("Enter an integer: ");
	scanf("%d", &x);
	//output
	printf("Result: %d\n", polynomial(x));	//calling
	
	return 0;
}
/*function defination*/
int  polynomial(int x){
	
	return 3*x*x*x*x*x + 2*x*x*x*x - 5*x*x*x - x*x + 7*x - 6;
}
