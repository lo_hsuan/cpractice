/*****************************************************************/
/* Class: Computer Programming, Fall 2019                        */
/* Author: 羅羽軒 (put your name here)                           */
/* ID: 108820001 (your student ID here)                          */
/* Date: 2019.11.7 (put program development started date here   */
/* Purpose: random walk (function)                               */
/* Change History: 1107                                          */
/*****************************************************************/
#include<stdio.h> 
#include<stdlib.h>		//srand()	rand()   
#include<time.h>		//time() 參數種子，以目前時間導入 -> srand(time(NULL)) 

#define N 10
#define FILLER '.'
/*function prototype*/
void generate_randome_walk(char walk[10][10]);
void print_array(char walk[10][10]);

/*main function*/
int main(void){
	char walk[N][N];
	//calling function
	generate_randome_walk(walk);
	print_array(walk);		
		
	return 0;
}

/*function definition*/
void generate_randome_walk(char walk[10][10]){
	char letter = 'A';
	int x, y, direction;			//new_x_y 正確才存入x y 
	int moves_tried, new_x, new_y;	//嘗試(確定)方向 
	//全部初始化為 .  
	for(x=0 ; x<N ; x++){
		for(y=0 ; y<N ; y++){
			walk[x][y]=FILLER;
		}
	}
		
	srand((unsigned) time(NULL));		//srand只需在程式開頭執行一次，之後的亂數就會不同 
	//第一項 
	x=0;
	y=0;
	walk[x][y]=letter++;				//a[0][0]=A   (先存 A  leter再++ 
	direction = rand() % 4;			//0-3亂數生成、方向選擇 
	moves_tried = 0;				
	
	while(moves_tried < 4 && letter <= 'Z'){		//try 4 次即為所有方向  
		switch((direction + moves_tried) % 4){
			case 0 : new_y = y-1 ; break;	//up
			case 1 : new_y = y+1 ; break;	//down
			case 2 : new_x = x+1 ; break;	//right
			case 3 : new_x = x-1 ; break;	//left
		}
		
		if(new_x >= 0&& new_y >= 0 && 			//確定新座標未超過邊界(0-4) 
		   new_x < 10 && new_y < 10 && 
		   walk[new_x][new_y] == FILLER){			//確定新座標沒走過 
			//printf("%d(o)\n", (direction + moves_tried) % 4);
							
			x = new_x;
			y = new_y;
			walk[x][y] = letter++;
			direction = rand() % 4;
			moves_tried = 0;
			
		}else{					//此處不可走 
		
			//printf("%d(x) ",(direction + moves_tried) % 4);
			moves_tried++;
			new_y = y;			//設回原始值 
			new_x = x;

		}
	}
}

void print_array(char walk[10][10]){
	int x,y;
	for(x=0; x<N ; x++){
		for(y=0; y<N ;y++){
			printf("%c ",walk[x][y]);
		}
		printf("\n");
	}
}
